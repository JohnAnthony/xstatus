module jo.hnanthony.com/xstatus

go 1.18

require (
	code.cloudfoundry.org/bytefmt v0.0.0-20211005130812-5bb3c17173e5
	github.com/BurntSushi/xgb v0.0.0-20210121224620-deaf085860bc
	github.com/distatus/battery v0.10.0
)

require (
	golang.org/x/sys v0.0.0-20220829200755-d48e67d00261 // indirect
	howett.net/plist v1.0.0 // indirect
)
